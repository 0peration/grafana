# grafana

[![Version: 1.0.2](https://img.shields.io/badge/Version-1.0.2-informational?style=flat-square) ](#)
[![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ](#)
[![AppVersion: 7.3.6](https://img.shields.io/badge/AppVersion-7.3.6-informational?style=flat-square) ](#)
[![Artifact Hub: kube-ops](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/kube-ops&style=flat-square)](https://artifacthub.io/packages/helm/kube-ops/grafana)

Grafana is an open source, feature rich metrics dashboard and graph editor for Graphite, Elasticsearch, OpenTSDB, Prometheus and InfluxDB.

## Installing the Chart

To install the chart with the release name `my-release`:

```console
$ helm repo add kube-ops https://charts.kube-ops.io
$ helm repo update
$ helm upgrade my-release kube-ops/grafana --install --namespace my-namespace --create-namespace --wait
```

## Uninstalling the Chart

To uninstall the chart:

```console
$ helm uninstall my-release --namespace my-namespace
```

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| adminPassword | string | `""` |  |
| adminUser | string | `"admin"` |  |
| affinity | object | `{}` | Affinity for pod assignment ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity |
| annotations | object | `{}` |  |
| dnsConfig | object | `{}` |  |
| dnsPolicy | string | `""` |  |
| ephemeralContainers | list | `[]` |  |
| extraArgs | list | `[]` | Additional CLI arguments for application |
| extraContainers | list | `[]` | Specifies additional containers |
| extraEnvVars | list | `[]` | Additional environment variables |
| extraVolumeMounts | list | `[]` |  |
| extraVolumes | list | `[]` | Additional mounts for application |
| fullnameOverride | string | `""` | Overrides the full name |
| global.imagePullPolicy | string | `"IfNotPresent"` | Image download policy ref: https://kubernetes.io/docs/concepts/containers/images/#updating-images |
| global.imagePullSecrets | list | `[]` | List of the Docker registry credentials ref: https://kubernetes.io/docs/concepts/containers/images/#updating-images |
| hostAliases | list | `[]` |  |
| httpPort | int | `3000` |  |
| image.repository | string | `"docker.io/grafana/grafana"` | Overrides the image repository |
| image.tag | string | `""` | Overrides the image tag whose default is the chart appVersion. |
| ingress.annotations | object | `{}` |  |
| ingress.className | string | `""` |  |
| ingress.enabled | bool | `false` | Specifies whether a ingress should be created |
| ingress.hosts[0].host | string | `"chart-template.example.com"` |  |
| ingress.hosts[0].paths[0] | string | `"/"` |  |
| ingress.labels | object | `{}` |  |
| ingress.tls | list | `[]` |  |
| initContainers | list | `[]` | Specifies init containers |
| labels | object | `{}` |  |
| nameOverride | string | `""` | Overrides the chart name |
| networkPolicy.annotations | object | `{}` | Annotations for NetworkPolicy |
| networkPolicy.egress | list | `[{}]` | Egress rules |
| networkPolicy.enabled | bool | `false` | Specifies whether a NetworkPolicy should be created |
| networkPolicy.ingress | list | `[{}]` | Ingress rules |
| networkPolicy.labels | object | `{}` | Additional labels for NetworkPolicy |
| nodeSelector | object | `{}` |  |
| pdb.enabled | bool | `false` | Specifies whether a pod disruption budget should be created |
| persistence.accessModes[0] | string | `"ReadWriteOnce"` |  |
| persistence.annotations | object | `{}` |  |
| persistence.dataSource | object | `{}` |  |
| persistence.enabled | bool | `false` |  |
| persistence.existingClaim | string | `""` |  |
| persistence.selector | object | `{}` |  |
| persistence.size | string | `"8Gi"` |  |
| persistence.storageClassName | string | `""` |  |
| persistence.volumeMode | string | `"Filesystem"` |  |
| podAnnotations | object | `{}` |  |
| podLabels | object | `{}` |  |
| podSecurityContext | object | `{"fsGroup":472,"runAsNonRoot":true}` | Pod security settings |
| podSecurityPolicy.annotations | object | `{}` |  |
| podSecurityPolicy.create | bool | `true` | Specifies whether a pod security policy should be created |
| podSecurityPolicy.enabled | bool | `true` | Specifies whether a pod security policy should be enabled |
| podSecurityPolicy.name | string | `""` | The name of the pod security policy to use. If not set and create is true, a name is generated using the fullname template |
| postStartHook | object | `{}` | This hook is executed immediately after a container is created. However, there is no guarantee that the hook will execute before the container ENTRYPOINT. |
| preStopHook | object | `{}` | This hook is called immediately before a container is terminated due to an API request or management event such as liveness probe failure, preemption, resource contention and others. ref: https://kubernetes.io/docs/concepts/containers/container-lifecycle-hooks/#container-hooks |
| priority | int | `0` |  |
| priorityClassName | string | `""` | Overrides default priority class ref: https://kubernetes.io/docs/concepts/configuration/pod-priority-preemption/ |
| rbac.annotations | object | `{}` |  |
| rbac.create | bool | `true` | Specifies whether a cluster role should be created |
| rbac.name | string | Generated using the fullname template | The name of the cluster role to use. |
| replicas | int | `1` | Replicas count |
| resources | object | `{}` |  |
| revisionHistoryLimit | int | `10` | specifies the number of old ReplicaSets to retain to allow rollback ref: https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#revision-history-limit |
| runtimeClassName | string | `""` | Overrides default runtime class |
| schedulerName | string | `""` | Overrides default scheduler |
| service.annotations | object | `{}` | Annotations for Service resource |
| service.clusterIP | string | `""` | Exposes the Service on a cluster IP ref: https://kubernetes.io/docs/concepts/services-networking/service/#choosing-your-own-ip-address |
| service.externalTrafficPolicy | string | `"Cluster"` | If you set service.spec.externalTrafficPolicy to the value Local, kube-proxy only proxies proxy requests to local endpoints, and does not forward traffic to other nodes. This approach preserves the original source IP address. If there are no local endpoints, packets sent to the node are dropped, so you can rely on the correct source-ip in any packet processing rules you might apply a packet that make it through to the endpoint. ref: https://kubernetes.io/docs/tutorials/services/source-ip/#source-ip-for-services-with-type-nodeport |
| service.httpNodePort | int | `30080` | Node port number (service.type==NodePort) |
| service.httpPort | int | `80` | HTTP port number |
| service.labels | object | `{}` | Additional labels for Service resource |
| service.loadBalancerIP | string | `""` | Only applies to Service Type: LoadBalancer LoadBalancer will get created with the IP specified in this field. |
| service.loadBalancerSourceRanges | list | `[]` | If specified and supported by the platform, this will restrict traffic through the cloud-provider load-balancer will be restricted to the specified client IPs. ref: https://kubernetes.io/docs/tasks/access-application-cluster/configure-cloud-provider-firewall/ |
| service.sessionAffinity | string | `"None"` | Supports "ClientIP" and "None". Used to maintain session affinity. |
| service.sessionAffinityConfig | object | `{"clientIP":{"timeoutSeconds":10800}}` | Contains the configurations of session affinity. Only used if service.sessionAffinity == ClientIP service.sessionAffinityConfig.clientIP.timeoutSeconds -- The value must be >0 && <=86400 |
| service.topology | bool | `false` | enables a service to route traffic based upon the Node topology of the cluster ref: https://kubernetes.io/docs/concepts/services-networking/service-topology/#using-service-topology Kubernetes >= kubeVersion 1.18 |
| service.topologyKeys | list | `[]` | A preference-order list of topology keys which implementations of services should use to preferentially sort endpoints when accessing this Service, it can not be used at the same time as externalTrafficPolicy=Local ref: https://kubernetes.io/docs/concepts/services-networking/service-topology/#using-service-topology |
| service.type | string | `"ClusterIP"` | Kubernetes ServiceTypes allow you to specify what kind of Service you want ref: https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types |
| serviceAccount.annotations | object | `{}` | Annotations to add to the service account |
| serviceAccount.create | bool | `true` | Specifies whether a service account should be created |
| serviceAccount.labels | object | `{}` | Labels to add to the service account |
| serviceAccount.name | string | `""` | The name of the service account to use. If not set and create is true, a name is generated using the fullname template |
| serviceMonitor.annotations | object | `{}` |  |
| serviceMonitor.enabled | bool | `false` | Specifies whether a ServiceMonitor should be created (prometheus operator CRDs required) |
| serviceMonitor.honorLabels | bool | `false` |  |
| serviceMonitor.interval | string | `"30s"` |  |
| serviceMonitor.jobLabel | string | `"app.kubernetes.io/name"` |  |
| serviceMonitor.labels | object | `{}` |  |
| serviceMonitor.metricRelabelings | list | `[]` |  |
| serviceMonitor.namespace | string | `"monitoring"` |  |
| serviceMonitor.path | string | `"/metrics"` |  |
| serviceMonitor.scrapeTimeout | string | `"30s"` |  |
| startupProbe | object | `{}` |  |
| terminationGracePeriodSeconds | int | `4800` | Grace period before the Pod is allowed to be forcefully killed ref: https://kubernetes.io/docs/concepts/containers/container-lifecycle-hooks/ |
| tolerations | list | `[]` |  |
| topologySpreadConstraints | list | `[]` | control how Pods are spread across your cluster among failure-domains such as regions, zones, nodes, and other user-defined topology domains. ref: https://kubernetes.io/docs/concepts/workloads/pods/pod-topology-spread-constraints/ |
| updateStrategy.type | string | `"Recreate"` |  |

## Requirements

Kubernetes: `>= 1.18.0`

| Repository | Name | Version |
|------------|------|---------|
| https://charts.kube-ops.io | generate | ~0.2.3 |

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.4.0](https://github.com/norwoodj/helm-docs/releases/v1.4.0)
